# Polycopié d'Algèbre I

Ce projet sera l'endroit où le polycopié écrit par les étudiants sera mis à jour. Pour nous aider à avancer, vous pouvez ouvrir une nouvelle issue dans l'onglet issues et y indiquer quels changements vous pensez sont nécessaires pour rendre le polycopié plus clair. Si vous avez des notes vous pouvez bien sûr également les partager par ce moyen.\
La dernière version du polycopié est téléchargeable [ici](https://gitlab.unige.ch/api/v4/projects/9129/jobs/artifacts/main/raw/main.pdf?job=build) ou peut être consultée [ici](https://gitlab.unige.ch/algebre-i-2023/polycopie/-/jobs/artifacts/main/browse?job=build)

Pour ceux qui savent utiliser LaTeX, vous êtes les bienvenus pour faire avancer le projet en créant de nouvelles merge requests. On vous demande juste de suivre le format proposé par [kaobook](https://www.latextemplates.com/template/kaobook).\
Il y a un dossier .vscode inclus dans le projet qui contient les configurations nécéssaires pour compiler le projet si vous utilisez VSCode et l'extension LaTeX Workshop (le projet prend quand même du temps à compiler donc soyez patients).\
Le projet est organisé en plusieurs sous-sections donc pas besoin d'avoir un seul fichier de +3000 lignes sans queue ni tête. Le chapitre sur les groupes est déjà implémenté pour vous donner un exemple.

Idéalement le polycopié devrait avoir un ordre logique (on n'utilise rien avant de l'avoir défini/prouvé) pour que chacun puisse lire le polycopié et savoir exactement de quoi on parle à tout moment. Le but est de pouvoir mieux comprendre les sujets abordés en cours pas de mémoriser leur ordre. C'est pourquoi l'ordre des sujets ne suivra pas nécessairement celui du cours mais ce n'est pas un drame parce qu'on est sensé recevoir le polycopié de Mme. Smirnova qui, je pense, est dans l'ordre du cours.

Si vous avez des questions n'hésitez pas à utiliser le groupe Telegram

:warning: **Pour les scribes**: Afin de se coordonner et ne pas se marcher sur les pattes je vous propose de créer ou vous assigner à une issue ouverte montrant clairement que vous travaillez sur telle ou telle partie. p.ex Si vous voulez écrire la partie sur les nombres complexes, vous créez une issue appelée "Nombres Complexes" ou qqch du style et vous vous y assignez. Comme ça chacun saura qu'il n'y a pas besoin d'écrire cette partie. Ensuite, une fois les vérifications faites, on fera le merge de votre code.
